Figure2
================
Enrichetta Mileti
03/03/2021

### This script will generate Figure 2 panel

#### Load Libraries

``` r
library(ggpubr)
library(ggplot2)
library(dplyr)
library(scales)
library(reshape2)
library(tidyverse)
```

#### Load data

``` r
load("/Data/ExpressionData/ExpressionTables.RData")
load("/Data/Additional Data/DE/DE_23NO_23OB_23POB.RData")
load("/Data/Cohort/Cohort.RData")
```

#### TC Annotation

``` r
load("/Data/Annotation/Annotation.RData")
```

#### DE tables

Extract only DE tc. It also create additional 3 dataframes, one per
condition(NO,OBESE and POST-OBESE)

``` r
glm<-tab

DE.tab<-function(tabs){
  DE<-tabs[tabs$FDR<0.05,]
  DE<-merge(DE,annotation,by="TC")
  
  return(DE)
}

DE.tab.specific<-function(tabs,contrast){
  out<-tabs[tabs$Contrast==contrast,]
  out$Sign<-ifelse(out$FDR<0.05,"sign","no")
  out<-merge(out,annotation,by="TC")
  out$SignUpDown<-ifelse((out$Sign=="sign" & out$updown=="up"),"up",ifelse((out$Sign=="sign" & out$updown=="down"),"down","non-significant"))
  out$log10FDR<-(-log10(out$FDR))
  return(out)
}
```

#### Overall DE

``` r
DE.glm<-DE.tab(glm)
```

#### NO DE tables

``` r
NO.glm<-DE.tab.specific(glm,"HivsF.NO")
```

#### OBESE DE tables

``` r
OB.glm<-DE.tab.specific(glm,"HivsF.OB.0")
```

#### POST-OBESE DE tables

``` r
POB.glm<-DE.tab.specific(glm,"HivsF.OB.2")
```

#### Add Common\_Specific to the overall DE table

``` r
NO_POB<-(intersect(NO.glm[NO.glm$FDR<0.05,]$TC,POB.glm[POB.glm$FDR<0.05,]$TC))
NO_OB<-(intersect(NO.glm[NO.glm$FDR<0.05,]$TC,OB.glm[OB.glm$FDR<0.05,]$TC))
OB_POB<-(intersect(OB.glm[OB.glm$FDR<0.05,]$TC,POB.glm[POB.glm$FDR<0.05,]$TC))
NO_OB_POB<-intersect(intersect(NO.glm[NO.glm$FDR<0.05,]$TC,OB.glm[OB.glm$FDR<0.05,]$TC),POB.glm[POB.glm$FDR<0.05,]$TC)

NO_POB_2<-NO_POB[!NO_POB %in% NO_OB_POB]
NO_OB_2<-NO_OB[!NO_OB %in% NO_OB_POB]
OB_POB_2<-OB_POB[!OB_POB %in% NO_OB_POB]


DE.glm$Comm_Spec<-ifelse(DE.glm$TC %in% NO_POB & (!(DE.glm$TC %in% NO_OB_POB)) ,"NO_POB"  
                         ,ifelse(DE.glm$TC %in% NO_OB & (!(DE.glm$TC %in% NO_OB_POB)),"NO_OB"
                                 ,ifelse(DE.glm$TC %in% OB_POB & (!(DE.glm$TC %in% NO_OB_POB)),"OB_POB"
                                         ,ifelse(DE.glm$TC %in% NO_OB_POB,"NO_OB_POB",DE.glm$Contrast))))
NO_spec<-DE.glm[DE.glm$Comm_Spec=="HivsF.NO",]$TC
OB_spec<-DE.glm[DE.glm$Comm_Spec=="HivsF.OB.0",]$TC
POB_spec<-DE.glm[DE.glm$Comm_Spec=="HivsF.OB.2",]$TC

NO.glm.sign<-unique(merge(NO.glm[NO.glm$FDR<0.05,],DE.glm[, c("TC", "Comm_Spec")], by = "TC"))
OB.glm.sign<-unique(merge(OB.glm[OB.glm$FDR<0.05,],DE.glm[, c("TC", "Comm_Spec")], by = "TC"))
POB.glm.sign<-unique(merge(POB.glm[POB.glm$FDR<0.05,],DE.glm[, c("TC", "Comm_Spec")], by = "TC"))

NO.glm.sign.2<-data.frame(unique(merge(NO.glm,DE.glm[, c("TC", "Comm_Spec")], by = "TC",all.x=T)))
OB.glm.sign.2<-unique(merge(OB.glm,DE.glm[, c("TC", "Comm_Spec")], by = "TC",all.x=T))
POB.glm.sign.2<-unique(merge(POB.glm,DE.glm[, c("TC", "Comm_Spec")], by = "TC",all.x=T))
```

## Figure 2A: Barplot of DE TC groups

``` r
bars<-function(tab){
  tab$Sign<-ifelse(tab$FDR<0.05,"sign","no")
  tab<-tab[tab$Sign=="sign",]
  p1<-dcast(tab,TC~Contrast,value.var="Contrast")
  colnames(p1)<-c("TC","NO","OB","POB")
  p1$NO[!is.na(p1$NO)]<-"NO"
  p1$OB[!is.na(p1$OB)]<-"OB"
  p1$POB[!is.na(p1$POB)]<-"POB"
  res.count <-p1%>%group_by(NO,OB,POB)%>%tally()
  res.count$type<-paste(paste(res.count$NO,res.count$OB,sep="@"),res.count$POB,sep="@")
  res.count.m<-melt(res.count,id=c("type","n"))
  res.count.m<-res.count.m[!is.na(res.count.m$value),]
  res.count.m<-arrange(res.count.m,(type))
  res.count.m$type<-(as.factor(res.count.m$type))
  res.count.m$type <- factor(res.count.m$type, levels=(levels(res.count.m$type)))
  bar<-ggbarplot(res.count.m, x = "variable", y = "n",fill="type",label=T,ylab="Number of DE TCs",
                 palette =c("#7fbc41", "#CC0000","#4C4CFF","#dd3497","#FFD64D",'white',"#FF8C00")
  )+border()+rremove('xlab')+rremove("legend")+rremove('ylab')
  bar
}
Figure2A<-bars(DE.glm)
Figure2A
```

![](/Other/Figure2/Figure2-unnamed-chunk-11-1.png)<!-- -->

#### Extract number of genes

``` r
genes_groups<-function(tab){
  tab$Sign<-ifelse(tab$FDR<0.05,"sign","no")
  tab<-tab[tab$gene!="NA",]
  p1<-dcast(tab,gene~Contrast,value.var="Contrast")
  colnames(p1)<-c("Gene_Symbol","NO","OB","POB")
  p1$NO[(p1$NO)>0]<-"NO"
  p1$OB[(p1$OB)>0]<-"OB"
  p1$POB[(p1$POB)>0]<-"POB"
  res.count <-p1%>%group_by(NO,OB,POB)%>%summarise(n=n_distinct(Gene_Symbol))
  res.count
}
Figure2A_genes<-genes_groups(DE.glm)
Figure2A_genes
```

    ## # A tibble: 7 x 4
    ## # Groups:   NO, OB [4]
    ##   NO    OB    POB       n
    ##   <chr> <chr> <chr> <int>
    ## 1 0     0     POB     102
    ## 2 0     OB    0         4
    ## 3 0     OB    POB       5
    ## 4 NO    0     0        24
    ## 5 NO    0     POB      78
    ## 6 NO    OB    0         4
    ## 7 NO    OB    POB      57

## Figures 2B, 2C, 2D: Volcano plots

Volcano Plot, color based on group subdivision (Common, POBspecific..)

``` r
volcano_plot<-function(data,xlim,ylim,palette){
  data$Comm_Spec[which(data$FDR>0.05)] = "non_significant"
  data$log10FDR<-(-log10(data$FDR))
  ggscatter(data,x="logFC",y="log10FDR"
            ,fill="Comm_Spec",alpha=0.8,palette = palette,shape = 21,size="logCPM"
            ,label ="gene",label.select = list(criteria = "`y`>=60"),repel=T,font.legend = c(14, "plain", "black")
            ,ylab="-log10(FDR)",xlab="logFC(hi/f)"
  )+xlim(-5,xlim)+ylim(0,ylim)+border()+geom_hline(yintercept=1.3)+geom_vline(xintercept=0,linetype = "dashed")+
    theme(legend.position = "top",
          legend.box = "vertical",
          legend.box.just="left") +guides(fill=guide_legend(nrow=3,byrow=TRUE))+ 
    scale_size_continuous(range = c(1,6))+rremove("legend")+rremove("ylab")+rremove("xlab")
  
}
```

Set color palette

``` r
palette_NO.sign<-c("#dd3497","white","#FF8C00","#FFD64D","grey")
palette_OB.sign<-c("#CC0000","white","#FF8C00","grey","#4C4CFF")
palette_POB.sign<-c("#7fbc41","#FF8C00","#FFD64D","grey","#4C4CFF")
```

## Figure 3B

``` r
Figure2B<-volcano_plot(OB.glm.sign.2,7.5,16.5,palette_OB.sign)
```

    ## Warning: `filter_()` is deprecated as of dplyr 0.7.0.
    ## Please use `filter()` instead.
    ## See vignette('programming') for more help
    ## This warning is displayed once every 8 hours.
    ## Call `lifecycle::last_warnings()` to see where this warning was generated.

``` r
Figure2B
```

![](/Other/Figure2/Figure2-unnamed-chunk-15-1.png)<!-- -->

## Figure 2C

``` r
Figure2C<-volcano_plot(POB.glm.sign.2,7.5,16.5,palette_POB.sign)
Figure2C
```

![](/Other/Figure2/Figure2-unnamed-chunk-16-1.png)<!-- -->

## Figure 2D

``` r
Figure2D<-volcano_plot(NO.glm.sign.2,7.5,16.5,palette_NO.sign)
Figure2D
```

![](/Other/Figure2/Figure2-unnamed-chunk-17-1.png)<!-- -->

#### Marginal densities Volcano

``` r
volcano_density<-function(df,type,palette,xlim){
  df<-df[!df$SignUpDown=="non-significant",]
    ggdensity(df[df$SignUpDown==type,],x="log10FDR",y = "..scaled..",palette=palette,
            color = "Comm_Spec",orientation="horizontal")+xlim(0,xlim)+
    border()+ylim(0,1)+rotate_x_text()+geom_vline(xintercept=1.3)
}
```

Set Palette

``` r
palette_POB.sign2<-c("#7fbc41","#FF8C00","#FFD64D","#4C4CFF")
palette_NO.sign2<-c("#dd3497","white","#FF8C00","#FFD64D")
palette_OB.sign2<-c("#CC0000","white","#FF8C00","#4C4CFF")
palette_OB.sign2_down<-c("white","#FF8C00","#4C4CFF")
```

## Density up and down regulated TC in OB

``` r
Figure2B_density_up<-volcano_density(OB.glm.sign.2,"up",palette_OB.sign2,16.5)
Figure2B_density_up+rremove("legend")
```

![](/Other/Figure2/Figure2-unnamed-chunk-20-1.png)<!-- -->

``` r
Figure2B_density_down<-volcano_density(OB.glm.sign.2,"down",palette_OB.sign2_down,16.5)+ scale_y_reverse()
Figure2B_density_down+rremove("legend")
```

![](/Other/Figure2/Figure2-unnamed-chunk-20-2.png)<!-- -->

## Density up and down regulated TC in POB

``` r
Figure2C_density_up<-volcano_density(POB.glm.sign.2,"up",palette_POB.sign2,16.5)
Figure2C_density_up+rremove("legend")
```

![](/Other/Figure2/Figure2-unnamed-chunk-21-1.png)<!-- -->

``` r
Figure2C_density_down<-volcano_density(POB.glm.sign.2,"down",palette_POB.sign2,16.5)+ scale_y_reverse()
Figure2C_density_down+rremove("legend")
```

![](/Other/Figure2/Figure2-unnamed-chunk-21-2.png)<!-- -->

## Density up and down regulated TC in NO

``` r
Figure2D_density_up<-volcano_density(NO.glm.sign.2,"up",palette_NO.sign2,16.5)
Figure2D_density_up+rremove("legend")
```

![](/Other/Figure2/Figure2-unnamed-chunk-22-1.png)<!-- -->

``` r
Figure2D_density_down<-volcano_density(NO.glm.sign.2,"down",palette_POB.sign2,16.5)+ scale_y_reverse()
Figure2D_density_down+rremove("legend")
```

![](/Other/Figure2/Figure2-unnamed-chunk-22-2.png)<!-- -->
