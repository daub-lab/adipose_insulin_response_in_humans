# Data Analysis for Phase2 Project
#
# This script will do Differential expression Analysis for 26 NO vs 26 OB vs 26 POB
# The DE of insulin responses
# 
# Author: Enrichetta Mileti
########################## Load Libraries ########################

library(ggpubr)
library(ggplot2)
library(dplyr)
library(tidyr)
#library(tidylog)
library(scales)
library(reshape2)
library(tidyverse)

########################## Load data #################################

### Load Expression data enad cohort details
load("/Data/ExpressionData/ExpressionData.RData")
load("/Data/Cohort/Cohort.RData")

### Load DE results 
load("/Data/Additional Data/DE/DE_23NO_23OB_23POB.RData")

##### TC Annotation ####
load("/Data/Annotation/Annotation.RData")


# Extract Sum expression for each category

phase2.tpm.m<-melt(phase2.female.tpm)
phase2.tpm.m<-merge(phase2.tpm.m,cohort[,c("Cond","newcond3","Patnr","OB_NO_POB")],by.x="Var2",by.y="Cond")
phase2.tpm.m<-merge(phase2.tpm.m,annotation[,c("TC","gene")],by.x="Var1",by.y="TC")

phase2.tpm.sumExp<-aggregate(value ~ Var1 +gene+ OB_NO_POB, data=phase2.tpm.m, FUN=sum)

phase2.tpm.sumExp.2<-aggregate(value ~ gene+ OB_NO_POB, data=phase2.tpm.m, FUN=sum)

phase2.tpm.sumExp.3<-merge(phase2.tpm.sumExp,phase2.tpm.sumExp.2,by=c("gene","OB_NO_POB"))
phase2.tpm.sumExp.3$Percentage_contribution<-(phase2.tpm.sumExp.3$value.x/phase2.tpm.sumExp.3$value.y)*100

phase2.tpm.sumExp.4<-data.frame(TC=phase2.tpm.sumExp.3$Var1,Gene_Symbol=phase2.tpm.sumExp.3$gene,Class=phase2.tpm.sumExp.3$OB_NO_POB,Percentage_contribution=phase2.tpm.sumExp.3$Percentage_contribution)



########################## DE tables ##########################
# Extract only DE tc. It also create additional 3 dataframes, one per condition(NO,OBESE and POST-OBESE)
glm<-tab

DE.tab<-function(tabs){
  DE<-tabs[tabs$FDR<0.05,]
  DE<-merge(DE,annotation,by="TC")
  
  return(DE)
}

DE.tab.specific<-function(tabs,contrast,type){
  out<-tabs[tabs$Contrast==contrast,]
  out$Sign<-ifelse(out$FDR<0.05,"sign","no")
  out<-merge(out,phase2.tpm.sumExp[phase2.tpm.sumExp$OB_NO_POB==type,],by.x="TC",by.y="Var1")
  out$score<-(sign(out$logFC)*(-log10(out$PValue)))
  out<-out[with(out,order(-out$value)),]
  return(out)
}

# Overall DE
DE.glm<-DE.tab(glm)

# NO table
NO.glm<-DE.tab.specific(glm,"HivsF.NO","NO")
NO.glm<-data.frame(NO.glm %>%
                     group_by(gene) %>%
                     mutate(ticker = row_number()))
NO.glm<-NO.glm[NO.glm$ticker==1,]

# OB Table
OB.glm<-DE.tab.specific(glm,"HivsF.OB.0","OB")
OB.glm<-data.frame(OB.glm %>%
                     group_by(gene) %>%
                     mutate(ticker = row_number()))
OB.glm<-OB.glm[OB.glm$ticker==1,]

# POST-OBESE DE tables
POB.glm<-DE.tab.specific(glm,"HivsF.OB.2","POB")
POB.glm<-data.frame(POB.glm %>%
                      group_by(gene) %>%
                      mutate(ticker = row_number()))
POB.glm<-POB.glm[POB.glm$ticker==1,]

# Ranked List
rank_list<-function(df){
  ranklist <- df$score
  names(ranklist)<-df$gene
  ranklist <- sort(ranklist, decreasing = TRUE)
}


# NO ranked list
NO_ranked<-rank_list(NO.glm)


# OB ranked list
OB_ranked<-rank_list(OB.glm)


# POB ranked list
POB_ranked<-rank_list(POB.glm)


# load the geneset database
human_wiki_pathway_Bader_gmt <- read.gmt("/Data/Additional Data/GSEA/Human_WikiPathways_August_01_2020_symbol.gmt")

##########################################################
################# GSEA Analysis ##########################
##########################################################

#### GSEA NO #####

GSEA_NO <- GSEA(NO_ranked, TERM2GENE=human_wiki_pathway_Bader_gmt,pvalueCutoff = 1,seed =T)
# GSEA_results here is a gseaResult class. extract the Enriched data and output as a dataframe
# GSEA NO extract Results table
GSEA_NO_table <-GSEA_NO@result 

# GSEA NO extract Results with p.adj <0.3
GSEA_NO_table <-(GSEA_NO_table[GSEA_NO_table$p.adjust<0.3,])

# Create table as input for Cytoscape
GSEA_NO_table_cyto<-data.frame(ID=GSEA_NO_table$ID,Description=GSEA_NO_table$Description,p.Val=GSEA_NO_table$pvalue,FDR=GSEA_NO_table$p.adjust,genes=GSEA_NO_table$core_enrichment)
GSEA_NO_table_cyto<-GSEA_NO_table_cyto%>% add_column(phenotype = "", .after = "FDR")

# Create table with NES results
GSEA_NO_table_cyto$NES<-GSEA_NO_table$NES
write.table(noquote(GSEA_NO_table_cyto),file="/Data/Additional Data/GSEA/GSEA/GSEA_wiki_NO_Exp_score_withNES.txt",row.names = F,col.names = T,sep="\t",quote=F)


#### GSEA OB #####
GSEA_OB <- GSEA(OB_ranked, TERM2GENE=human_wiki_pathway_Bader_gmt, pvalueCutoff= 1,seed =T)

# GSEA_results here is a gseaResult class. extract the Enriched data and output as a dataframe
# GSEA OB extract Results table
GSEA_OB_table <-GSEA_OB@result

# GSEA OB extract Results with p.adj <0.3
GSEA_OB_table <-(GSEA_OB_table[GSEA_OB_table$p.adjust<0.3,])

# Create table as input for Cytoscape
GSEA_OB_table_cyto<-data.frame(ID=GSEA_OB_table$ID,Description=GSEA_OB_table$Description,p.Val=GSEA_OB_table$pvalue,FDR=GSEA_OB_table$p.adjust,genes=GSEA_OB_table$core_enrichment)
GSEA_OB_table_cyto<-GSEA_OB_table_cyto%>% add_column(phenotype = "", .after = "FDR")
write.table(noquote(GSEA_OB_table_cyto),file="/Data/Additional Data/GSEA/GSEA_wiki_OB_Exp_score.txt",row.names = F,col.names = T,sep="\t",quote=F)

# Create table with NES results
GSEA_OB_table_cyto$NES<-GSEA_OB_table$NES
write.table(noquote(GSEA_OB_table_cyto),file="/Data/Additional Data/GSEA/GSEA_wiki_OB_Exp_score_withNES.txt",row.names = F,col.names = T,sep="\t",quote=F)


#### GSEA POB #####
GSEA_POB <- GSEA(POB_ranked, TERM2GENE=human_wiki_pathway_Bader_gmt, pvalueCutoff= 1,seed =T)

# GSEA_results here is a gseaResult class. extract the Enriched data and output as a dataframe
# GSEA POB extract Results table
GSEA_POB_table <-GSEA_POB@result

# GSEA POB extract Results with p.adj <0.3
GSEA_POB_table <-(GSEA_POB_table[GSEA_POB_table$p.adjust<0.3,])

# Create table as input for Cytoscape
GSEA_POB_table_cyto<-data.frame(ID=GSEA_POB_table$ID,Description=GSEA_POB_table$Description,p.Val=GSEA_POB_table$pvalue,FDR=GSEA_POB_table$p.adjust,genes=GSEA_POB_table$core_enrichment)
GSEA_POB_table_cyto<-GSEA_POB_table_cyto%>% add_column(phenotype = "", .after = "FDR")
write.table(noquote(GSEA_POB_table_cyto),file="/Users/enrichettamileti/OneDrive - KI.SE/Share Carsten Enrichetta papers/WeightLoss_Sept2020/Data/GSEA/GSEA_wiki_POB_Exp_score.txt",row.names = F,col.names = T,sep="\t",quote=F)

# Create table with NES results
GSEA_POB_table_cyto$NES<-GSEA_POB_table$NES
write.table(noquote(GSEA_POB_table_cyto),file="/Users/enrichettamileti/OneDrive - KI.SE/Share Carsten Enrichetta papers/WeightLoss_Sept2020/Data/GSEA/GSEA_wiki_POB_Exp_score_withNES.txt",row.names = F,col.names = T,sep="\t",quote=F)



