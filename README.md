# Human white adipose tissue displays selective insulin resistance in the obese state

**Enrichetta Mileti<sup>1</sup>, Kelvin HM. Kwok<sup>1</sup>, Daniel P. Andersson<sup>2</sup>, Anthony Mathelier<sup>3,4</sup>, Amitha
Raman<sup>5</sup>, Jesper Bäckdahl<sup>2</sup>, Jutta Jalkanen<sup>2</sup>, Lucas Massier<sup>2</sup>, Anders Thorell<sup>6</sup>, Hui Gao<sup>1</sup>, Peter Arner<sup>2</sup>, Niklas Mejhert<sup>2</sup>, Carsten O. Daub<sup>1,5&ast;</sup>, Mikael Rydén<sup>2&ast;</sup>**

<sup>1</sup>Department of Biosciences and Nutrition, Karolinska Institutet, 141 83, Stockholm, Sweden.<br>
<sup>2</sup>Department of Medicine (H7), Huddinge, Karolinska Institutet, Karolinska University Hospital, 141 86, Stockholm, Sweden.<br>
<sup>3</sup>Centre for Molecular Medicine Norway (NCMM), Nordic EMBL Partnership, University of Oslo, 0318 Oslo, Norway.<br>
<sup>4</sup>Department of Medical Genetics, Institute of Clinical Medicine, University of Oslo and Oslo University Hospital, 0424, Oslo, Norway.<br>
<sup>5</sup>Science for Life Laboratory, Stockholm, Sweden.<br>
<sup>6</sup>Department of Clinical Sciences, Danderyd Hospital, Karolinska Institutet & Department of Surgery, Ersta Hospital, 116 28, Stockholm.<br>
<sup>*</sup>Shared correspondence

## Contacts
For bioinformatics-related questions, please contact:<br>
<a href="mailto:enrichettamileti@gmail.com">Enrichetta Mileti</a><br> 
<a href="mailto:carsten.daub@ki.se">Carsten Daub</a><br> 

For all questions regarding study design, please contact: <br>
<a href="mailto:mikael.ryden@ki.se">Mikael Rydén</a><br>
<a href="mailto:niklas.mejhert@ki.se">Niklas Mejhert</a><br> 


## Abstract
<div align="justify"> Selective hepatic insulin resistance is a feature of obesity and type 2 diabetes. Whether similar mechanisms operate in white adipose tissue (WAT) of obese subjects and to what extent these are normalized by weight loss is unknown. We determined insulin sensitivity by hyperinsulinemic euglycemic clamp and the insulin response in subcutaneous WAT by RNA-sequencing in 23 women with obesity before and two years after bariatric surgery. To control for effects of surgery, women post-surgery were matched to never-obese subjects. Multidimensional analyses of 138 samples allowed us to classify the effects of insulin into three distinct expression responses: a common set was present in all three groups and included genes encoding several lipid/cholesterol biosynthesis enzymes; a set of obesity-attenuated genes linked to tissue remodelling and protein translation was selectively regulated in the two non-obese states and several post obesity-enriched genes encoding proteins involved in e.g. one carbon metabolism were only responsive to insulin in the women who had lost weight. Altogether, human WAT displays a selective insulin response in the obese state where most genes are normalized by weight loss. This comprehensive atlas provides insights into the transcriptional effects of insulin in WAT and may identify targets to improve insulin action. </div>

## Study design
![](/Other/Figure_S1_R1.png)<!-- -->


## Data
Expression data, TC annotation and cohort description can be found here: <br>

1. [Expression Data](Data/ExpressionData/ExpressionTable.txt): TC expression table (raw count)<br>
2. [TC Annotation](Data/Annotation/Annotation.txt): TC gene annotation<br>
3. [Cohort](Data/Cohort/Cohort.txt): Cohort details<br>

Individual clinical data can be provided upon request (<a href="mailto:mikael.ryden@ki.se">Mikael Rydén</a>).

## Scripts for reproduction of manuscript figures
All figures (except those not prepared using R) in the manuscript can be reproduced from the R scripts within this repository.<br>

Main figures
-------------
**Figure 1A-F** [RScript](Scripts/Rscripts/Figure1AF.R) [Markdown](Scripts/Markdown/Figure1AF.md) Individual clinical data to reproduce Figure 1 A-F can be provided upon request <a href="mailto:mikael.ryden@ki.se">Mikael Rydén</a><br>
**Figure 1G-H** [RScript](Scripts/Rscripts/Figure1GH.R) [Markdown](Scripts/Markdown/Figure1GH.md) <br>
**Figure 2A-D** [RScript](Scripts/Rscripts/Figure2.R) [Markdown](Scripts/Markdown/Figure2.md)<br>
**Figure 3A-B** [RScript](Scripts/Rscripts/Figure3AB.R) [Markdown](Scripts/Markdown/Figure3AB.md)<br>
**Figure 3C-D** [RScript](Scripts/Rscripts/Figure3CD.R) [Markdown](Scripts/Markdown/Figure3CD.md)<br>


Supplementary figures
-------------
**Figure S2** [Rscript](Scripts/Rscripts/FigureS2.R) [Markdown](Scripts/Markdown/FigureS2.md)<br>
**Figure S3** [Rscript](Scripts/Rscripts/FigureS3.R) [Markdown](Scripts/Markdown/FigureS3.md)<br>
**Figure S5** [Rscript](Scripts/Rscripts/FigureS5.R) [Markdown](Scripts/Markdown/FigureS5.md)<br>

Additional scripts relevant for the study 
-----------------------------------------

[Differential Expression Analysis](Scripts/Rscripts/DifferentialExpression.R)<br>
[Gene Set Enrichment Analysis (GSEA)](Scripts/Rscripts/GSEA.R)<br>

Supplementary Tables
---------------------
[Table S1](/Data/Supplementary Tables/Table_S1.xlsx): TCs biotypes annotation.<br>
[Table S2](/Data/Supplementary Tables/Table_S2.docx): Clinical data.<br>
[Table S3](/Data/Supplementary Tables/Table_S3.txt): Differentially expressed insulin-regulated tag clusters and corresponding genes.<br>
[Table S4](/Data/Supplementary Tables/Table_S4.txt): Enriched transcription factor binding sites for the groups OB, POB and NO.<br>
[Table S5](/Data/Supplementary Tables/Table_S5.txt): Gene set enrichment analysis results.<br>
[Table S6](/Data/Supplementary Tables/Table_S6.txt): Enriched transcription factor binding sites for tag clusters related ribosomal proteins.<br>

## Links to relavant sites

[Full length manuscript](https://diabetesjournals.org/diabetes/article/70/7/1486/137854/Human-White-Adipose-Tissue-Displays-Selective)<br>
[Previous published manuscript related to this study](https://doi.org/10.1016/j.celrep.2016.07.070) <br>
[Daub Lab website](https://www.daublab.org)<br>
[Rydén-Mejhert Lab website](https://ki.se/en/medh/ryden-mejhert-group)<br>



